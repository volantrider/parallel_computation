﻿#include "pch.h"

#include "aligned_array_pointer_test.h"
#include "square_matrix_test.h"
#include "matrix_arithmetic_test.h"

int main()
{
	alignedArrayPointer_test();
	squareMatrix_test();
	matrixArithmetic_test();
}