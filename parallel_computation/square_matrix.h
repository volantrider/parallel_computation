#pragma once

#include <cstddef>
#include <type_traits>
#include <cassert>

template <class T>
class SquareMatrix
{
public:
	SquareMatrix() = delete;
	inline SquareMatrix(SquareMatrix<T>&& matrix) = delete; //TODO release
	inline explicit SquareMatrix(const SquareMatrix<T>& matrix);
	inline explicit SquareMatrix(std::size_t nAlignmentBytes, std::size_t dimension);
	inline ~SquareMatrix();

	inline SquareMatrix<T>& operator= (const SquareMatrix<T>& pointer);

	inline std::size_t getNAlignmentBytes() const;
	inline std::size_t getDimension() const;
	inline T * getAlignedPointer() const;
	inline T * getBasePointer() const;

	inline void resize(std::size_t nAlignmentBytes, std::size_t dimension);

	inline T* operator[](std::size_t iRow) const;

	void transpos();
	
private:
	inline void create();
	inline void remove();

	std::size_t nAlignmentBytes_;
	std::size_t dimension_;
	T* basePointer_;
	T* alignedPointer_;
};

template<class T>
inline bool operator == (const SquareMatrix<T>& matrix1, const SquareMatrix<T>& matrix2);

template<class T>
inline bool operator != (const SquareMatrix<T>& matrix1, const SquareMatrix<T>& matrix2);

template<class T>
inline void showAllToConsole(const SquareMatrix<T>& matrix);

template<class T>
inline void showElementsToConsole(const SquareMatrix<T>& matrix);


template<class T>
inline SquareMatrix<T>::SquareMatrix(const SquareMatrix<T>& matrix)
	: nAlignmentBytes_ {matrix.nAlignmentBytes_}, dimension_ {matrix.dimension_}
{
	create();
	this->operator=(matrix);
}
template<class T>
inline SquareMatrix<T>::SquareMatrix(std::size_t nAlignmentBytes, std::size_t dimension)
	: nAlignmentBytes_ {nAlignmentBytes}, dimension_ {dimension}
{
	create();
}

template<class T>
inline SquareMatrix<T>::~SquareMatrix()
{
	remove();
}

template<class T>
inline SquareMatrix<T>& SquareMatrix<T>::operator=(const SquareMatrix<T>& pointer)
{
	assert(pointer.getNAlignmentBytes() == nAlignmentBytes_);
	assert(pointer.getDimension() == dimension_);
	for (std::size_t i = 0; i < dimension_; ++i)
	{
		T* row = this->operator[](i);
		for (std::size_t j = 0; j < dimension_; ++j)
		{
			row[j] = pointer[i][j];
		}
	}
	return *this;
}

template<class T>
inline std::size_t SquareMatrix<T>::getNAlignmentBytes() const
{
	return nAlignmentBytes_;
}

template<class T>
inline std::size_t SquareMatrix<T>::getDimension() const
{
	return dimension_;
}

template<class T>
inline T * SquareMatrix<T>::getAlignedPointer() const
{
	return alignedPointer_;
}

template<class T>
inline T * SquareMatrix<T>::getBasePointer() const
{
	return basePointer_;
}

template<class T>
inline void SquareMatrix<T>::resize(std::size_t nAlignmentBytes, std::size_t dimension)
{
	remove();
	nAlignmentBytes_ = nAlignmentBytes;
	dimension_ = dimension;
	create();
}

template<class T>
inline T * SquareMatrix<T>::operator[](std::size_t iRow) const
{
	assert(iRow < dimension_);
	return alignedPointer_ + iRow * dimension_;
}

template<class T>
inline void SquareMatrix<T>::transpos()
{
	for (std::size_t i = 0; i < getDimension(); ++i)
	{
		T* matrixRow = operator[](i);
		for (std::size_t j = i + 1; j < getDimension(); ++j)			
		{
			T temp = matrixRow[j];
			matrixRow[j] = operator[](j)[i];
			operator[](j)[i] = temp;
		}
	}
}

template<class T>
inline void SquareMatrix<T>::create()
{
	static_assert (!std::is_class<T>::value);
	assert(dimension_);
	assert(nAlignmentBytes_);
	//assert(((nElements_ * sizeof (T)) % nAlignmentBytes_) == 0); //������� ������������ ������ �������
	std::size_t reserv = nAlignmentBytes_ % sizeof(T) == 0 ?
		nAlignmentBytes_ / sizeof(T) : nAlignmentBytes_ / sizeof(T) + 1;
	basePointer_ = new T[dimension_ * dimension_ + reserv];
	std::size_t alignedPointer = reinterpret_cast<std::size_t>(basePointer_);
	alignedPointer += (nAlignmentBytes_ - (alignedPointer % nAlignmentBytes_))
		% nAlignmentBytes_;
	//��� �� ������� 2: (% nAlignmentBytes) ����� �������� �� (& (nAlignmentBytes-1))
	alignedPointer_ = reinterpret_cast<T*>(alignedPointer);
}

template<class T>
inline void SquareMatrix<T>::remove()
{
	delete[] basePointer_;
}

template<class T>
inline bool operator==(const SquareMatrix<T>& matrix1, const SquareMatrix<T>& matrix2)
{
	if (matrix1.getDimension() != matrix2.getDimension())
	{
		return false;
	}
	for (std::size_t i = 0; i < matrix1.getDimension(); ++i)
	{
		T* matrix1Row = matrix1[i];
		T* matrix2Row = matrix2[i];
		for (std::size_t j = 0; j < matrix1.getDimension(); ++j)
		{
			if (matrix1Row[j] != matrix2Row[j])
			{
				return false;
			}
		}
	}
	return true;
}

template<class T>
inline bool operator!=(const SquareMatrix<T>& matrix1, const SquareMatrix<T>& matrix2)
{
	return !(matrix1 == matrix2);
}

template<class T>
inline void showAllToConsole(const SquareMatrix<T>& matrix)
{
	std::cout << "basePointer = " << matrix.getBasePointer() << std::endl;
	std::cout << "alignedPointer = " << matrix.getAlignedPointer() << std::endl;
	std::cout << "nAlignmentBytes = " << matrix.getNAlignmentBytes() << std::endl;
	std::cout << "dimension = " << matrix.getDimension() << std::endl << std::endl;
	showElementsToConsole(matrix);
}

template<class T>
inline void showElementsToConsole(const SquareMatrix<T>& matrix)
{
	for (std::size_t i = 0; i < matrix.getDimension(); ++i)
	{
		T* matrixRow = matrix[i];
		for (std::size_t j = 0; j < matrix.getDimension(); ++j)
		{				
			if constexpr (std::is_floating_point_v<T>)
			{
				printf("%8.2f ", matrixRow[j]);
			}
			else
			{
				printf("%4i ", matrixRow[j]);
			}
		}
		std::cout << std::endl;
	}
	std::cout << std::endl << std::endl;
}
