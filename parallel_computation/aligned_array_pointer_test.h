#pragma once

#include <iostream>

#include "aligned_array_pointer.h"

void alignedArrayPointer_test()
{
	std::cout << "__________________________________________________" << std::endl 
		<< "alignedArrayPointer_test" << std::endl << std::endl 
		<< "getBasePointer() getAlignedPointer() getNAlignmentBytes() getNElements()" << std::endl << std::endl;

	{
		//AlignedArrayPointer<int> pInt0(0, 10); //assert
		//AlignedArrayPointer<int> pInt8(8, 0); //assert
	}
	{
		std::cout << "pFloat16" << std::endl;
		//AlignedArrayPointer<float> pFloat16(16, 10); //assert
		AlignedArrayPointer<float> pFloat16(16, 4);	
		assert(reinterpret_cast<std::size_t>(pFloat16.getAlignedPointer()) % 16 == 0);
		for (std::size_t i = 0; i < pFloat16.getNElements(); ++i)
		{
			pFloat16[i] = i;			
		}
		pFloat16.showToConsole();		
	}
	{
		std::cout << "pDouble64" << std::endl;
		//AlignedArrayPointer<double> pDouble64(64, 1); //assert
		AlignedArrayPointer<double> pDouble64(64, 16);		
		assert(reinterpret_cast<std::size_t>(pDouble64.getAlignedPointer()) % 64 == 0);
		for (std::size_t i = 0; i < pDouble64.getNElements(); ++i)
		{
			pDouble64[i] = i * 2;
		}		
		pDouble64.showToConsole();
		//        pDouble64[16] = 5; //assert
		std::cout << "resize" << std::endl;
		pDouble64.resize(24, 2);		
		assert(reinterpret_cast<std::size_t>(pDouble64.getAlignedPointer()) % 24 == 0);
		pDouble64[1] = 42;
		pDouble64.showToConsole();
	}
	{
		std::cout << "pChar65" << std::endl;
		AlignedArrayPointer<double> pChar65(65, 130);
		assert(reinterpret_cast<std::size_t>(pChar65.getAlignedPointer()) % 65 == 0);
		for (std::size_t i = 0; i < pChar65.getNElements(); ++i)
		{
			pChar65[i] = i;
		}
		pChar65.showToConsole();
	}
	{		
		class Class_test { };
		//AlignedArrayPointer<Class_test> pStruct32_1(9, 10); //assert
	}
	std::cout << "__________________________________________________" << std::endl << std::endl << std::endl;
}
