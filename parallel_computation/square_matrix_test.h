#pragma once

#include <iostream>

#include "square_matrix.h"

void squareMatrix_test()
{
	std::cout << "__________________________________________________" << std::endl
		<< "squareMatrix_test" << std::endl << std::endl;

	std::cout << "m1(16, 5)" << std::endl;
	SquareMatrix<float> m1(16, 5);
	for (std::size_t i = 0; i < m1.getDimension(); ++i)
	{
		for (std::size_t j = 0; j < m1.getDimension(); ++j)
		{
			m1[i][j] = i * m1.getDimension() + j;
		}
	}
	showAllToConsole(m1);
	std::cout << "transpos" << std::endl;
	m1.transpos();
	showElementsToConsole(m1);

	SquareMatrix<float> m2(32, 5);
	//m2 = m1; //assert
	SquareMatrix<float> m3(16, 7);
	//m3 = m1;//assert

	std::cout << std::endl << "m4(4, 2)" << std::endl;
	SquareMatrix<long long> m4(4, 2);
	for (std::size_t i = 0; i < m4.getDimension(); ++i)
	{
		for (std::size_t j = 0; j < m4.getDimension(); ++j)
		{
			m4[i][j] = i * m4.getDimension() + j;
		}
	}
	showAllToConsole(m4);

	std::cout << std::endl << "m5(4, 2)" << std::endl;
	SquareMatrix<long long> m5(4, 2);
	for (std::size_t i = 0; i < m5.getDimension(); ++i)
	{
		for (std::size_t j = 0; j < m5.getDimension(); ++j)
		{
			m5[i][j] = i * m5.getDimension() + j + 1;
		}
	}
	showAllToConsole(m5);

	if (m4 == m5)
	{
		std::cout << "m4 == m5" << std::endl;
	}
	else
	{
		std::cout << "m4 != m5" << std::endl;
	}

	std::cout << std::endl << "m5 = m4" << std::endl;
	m5 = m4;
	showElementsToConsole(m5);
	if (m4 == m5)
	{
		std::cout << "m4 == m5" << std::endl;
	}
	else
	{
		std::cout << "m4 != m5" << std::endl;
	}

	std::cout << std::endl << "m5(m4)" << std::endl;
	SquareMatrix<long long> m6(m5);
	showAllToConsole(m5);
	if (m4 == m6)
	{
		std::cout << "m4 == m6" << std::endl;
	}
	else
	{
		std::cout << "m4 != m6" << std::endl;
	}
	std::cout << "__________________________________________________" << std::endl;
}