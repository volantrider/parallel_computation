#pragma once

#include <cstddef>
#include <type_traits>
#include <cassert>

//Процессор считывает данные из памяти блоками. Блок состовляет 2^k байт (классические процессоры).
//Выровненный адресс - адресс соответствующий началу блока памяти. 

//(выровненные данные имеют выровнненый адресс и размер кратный размеру блока)
//Процессоры медленно работают с невыровненными адрессами (некоторые процессроы не работают с ними вообще).
//SSE инструкции требуют выравнивания по границе 16 байт. (поэтому в программе указатель выравнивается).
//Поля структур в Си выравниваются компилятором (размер структуры обычно больше ожидаемого).


//Класс выравненного указателя массива.
//Реализован согласно патерну RAII.
//Внимание: память выделяется с запасом; T - не может быть классом (конструируемый объект).
template <class T>
class AlignedArrayPointer
{
public:
    AlignedArrayPointer() = delete;
    inline AlignedArrayPointer(AlignedArrayPointer<T>&& pointer) = delete; //TODO release
    inline explicit AlignedArrayPointer(const AlignedArrayPointer<T>& pointer) = delete;
    inline explicit AlignedArrayPointer(std::size_t nAlignmentBytes, std::size_t nElements = 1);
    inline ~AlignedArrayPointer();

    inline AlignedArrayPointer<T>& operator= (AlignedArrayPointer<T>& pointer) = delete;

    inline void resize(std::size_t nAlignmentBytes, std::size_t nElements = 0);

    inline std::size_t getNAlignmentBytes() const;
    inline std::size_t getNElements() const;
    inline T * getAlignedPointer() const;
    inline T * getBasePointer() const;

    inline T& operator[] (std::size_t index) const;

	inline void showToConsole() const;

private:
    inline void create();
    inline void remove();

    std::size_t nAlignmentBytes_;
    std::size_t nElements_;
    T* basePointer_;
    T* alignedPointer_;
};

template <class T>
AlignedArrayPointer<T>::AlignedArrayPointer(std::size_t nAlignmentBytes, std::size_t nElements)
    : nAlignmentBytes_ {nAlignmentBytes}, nElements_ {nElements}
{
    create();
}

template<class T>
AlignedArrayPointer<T>::~AlignedArrayPointer()
{
    remove();
}

template<class T>
void AlignedArrayPointer<T>::resize(std::size_t nAlignmentBytes, std::size_t nElements)
{
    remove();
    nAlignmentBytes_ = nAlignmentBytes;
    nElements_ = nElements;
    create();
}

template<class T>
inline std::size_t AlignedArrayPointer<T>::getNAlignmentBytes() const
{
    return nAlignmentBytes_;
}

template<class T>
inline std::size_t AlignedArrayPointer<T>::getNElements() const
{
    return nElements_;
}

template<class T>
inline T *AlignedArrayPointer<T>::getAlignedPointer() const
{
    return alignedPointer_;
}

template<class T>
inline T *AlignedArrayPointer<T>::getBasePointer() const
{
    return basePointer_;
}

template<class T>
T &AlignedArrayPointer<T>::operator[](std::size_t index) const
{
    assert(index < getNElements());
    return alignedPointer_[index];
}

template<class T>
inline void AlignedArrayPointer<T>::showToConsole() const
{
	std::cout << "basePointer = " << basePointer_ << std::endl;
	std::cout << "alignedPointer = " << alignedPointer_ << std::endl;
	std::cout << "nAlignmentBytes = " << nAlignmentBytes_ << std::endl;
	std::cout << "nElements = " << nElements_ << std::endl << std::endl;
	for (std::size_t i = 0; i < nElements_; ++i)
	{
		std::cout << this->operator[](i) << " ";
	}
	std::cout << std::endl << std::endl;
}

template<class T>
void AlignedArrayPointer<T>::create()
{
    static_assert (!std::is_class<T>::value);
    assert(nElements_);
    assert(nAlignmentBytes_);
    //assert(((nElements_ * sizeof (T)) % nAlignmentBytes_) == 0); //условие выравнивания правой границы
    std::size_t reserv = nAlignmentBytes_ % sizeof(T) == 0 ?
                nAlignmentBytes_ / sizeof(T) : nAlignmentBytes_ / sizeof(T) + 1;
    basePointer_ = new T[nElements_ + reserv];
    std::size_t alignedPointer = reinterpret_cast<std::size_t>(basePointer_);
	alignedPointer += (nAlignmentBytes_ - (alignedPointer % nAlignmentBytes_))
            % nAlignmentBytes_;
    //для СС кратных 2: (% nAlignmentBytes) можно заменить на (& (nAlignmentBytes-1))
    alignedPointer_ = reinterpret_cast<T*>(alignedPointer);
}

template<class T>
inline void AlignedArrayPointer<T>::remove()
{
	if (nElements_)
	{
		delete[] basePointer_;
	}
	else
	{
		delete basePointer_;
	}
}
