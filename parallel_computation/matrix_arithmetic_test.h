#pragma once

#include <iostream>
#include <fstream>

#include "matrix_arithmetic.h"
#include "aligned_array_pointer.h"

template <class T>
using pf = void(*)(const SquareMatrix<T>& matrix1, const SquareMatrix<T>& matrix2, SquareMatrix<T>& result);

template <class T>
using pf2 = void(*)(const SquareMatrix<T>& matrix1, const SquareMatrix<T>& matrix2, SquareMatrix<T>& result, std::size_t blockSize);

template <class T>
inline double mulTime_test(std::size_t nAlignmentBytes, std::size_t dimension, pf<T> f, std::size_t nLoops)
{
	std::cout << nAlignmentBytes << " " << dimension << std::endl;
	SquareMatrix<T> m1(nAlignmentBytes, dimension);
	for (std::size_t i = 0; i < m1.getDimension(); ++i)
	{
		for (std::size_t j = 0; j < m1.getDimension(); ++j)
		{
			m1[i][j] = i * m1.getDimension() + j;
		}
	}
	SquareMatrix<T> m2(m1);
	SquareMatrix<T> m3(nAlignmentBytes, dimension);

	double time0 = omp_get_wtime();
	for (int q = 0; q < nLoops; ++q)
	{
		f(m1, m2, m3);
	}
	double t = omp_get_wtime() - time0;
	printf("%lg\n", t);
	return t;
}

template <class T>
double matrixBlockMulTime_test(std::size_t nAlignmentBytes, std::size_t dimension, std::size_t blockSize, pf2<T> f, std::size_t nLoops)
{

	std::cout << nAlignmentBytes << " " << dimension << " " << blockSize << std::endl;
	SquareMatrix<T> m1(nAlignmentBytes, dimension);
	for (std::size_t i = 0; i < m1.getDimension(); ++i)
	{
		for (std::size_t j = 0; j < m1.getDimension(); ++j)
		{
			m1[i][j] = i * m1.getDimension() + j;
		}
	}
	SquareMatrix<T> m2(m1);
	SquareMatrix<T> m3(nAlignmentBytes, dimension);

	double time0 = omp_get_wtime();
	for (int q = 0; q < nLoops; ++q)
	{
		f(m1, m2, m3, blockSize);
	}
	double t = omp_get_wtime() - time0;
	printf("%lg\n", t);
	return t;
}

void matrixArithmetic_test()
{
	std::cout << "__________________________________________________" << std::endl
		<< "matrixArithmetic_test" << std::endl << std::endl;
	{
		//����������� ��������� ���������
		std::cout << std::endl << "_________________" << std::endl << "Verify" << std::endl;
		const std::size_t DIMENSION = 4;
		SquareMatrix<float> m1(16, DIMENSION);
		for (std::size_t i = 0; i < m1.getDimension(); ++i)
		{
			for (std::size_t j = 0; j < m1.getDimension(); ++j)
			{
				m1[i][j] = i * m1.getDimension() + j;
			}
		}
		std::cout << "m1" << std::endl;
		showElementsToConsole(m1);

		SquareMatrix<float> m2(m1);
		std::cout << "m2" << std::endl;
		showElementsToConsole(m2);

		SquareMatrix<float>m3(m1);
		multiplySquareMatrix(m1, m2, m3);
		std::cout << "SympleMul m3" << std::endl;
		showElementsToConsole(m3);
		SquareMatrix<float>m4(m1);
		multiplySquareMatrix4x4asm(m1, m2, m4);
		std::cout << "SseMul m4" << std::endl;
		if (m3 == m4)
		{
			std::cout << "m3 == m4" << std::endl;
		}
		else
		{
			std::cout << "m3 != m4" << std::endl;
		}
		SquareMatrix<float>m5(m1);
		multiplySquareMatrixOmp(m1, m2, m5);
		std::cout << "OmpMul m5" << std::endl;
		if (m3 == m5)
		{
			std::cout << "m3 == m5" << std::endl;
		}
		else
		{
			std::cout << "m3 != m5" << std::endl;
		}

		std::cout << "TransMul m6" << std::endl;
		SquareMatrix<float>m6(m1);
		multiplyTransposMatrix(m1, m2, m6);
		if (m3 == m6)
		{
			std::cout << "m3 == m6" << std::endl;
		}
		else
		{
			std::cout << "m3 != m6" << std::endl;
		}
		std::cout << "TransParMul m7" << std::endl;
		SquareMatrix<float>m7(m1);
		multiplyTransposMatrixOmp(m1, m2, m7);
		if (m3 == m7)
		{
			std::cout << "m3 == m7" << std::endl;
		}
		else
		{
			std::cout << "m3 != m7" << std::endl;
		}
		std::cout << "BlockMul m8" << std::endl;
		SquareMatrix<float>m8(m1);
		multiplyBlockMatrix(m1, m2, m8, 2);
		if (m3 == m8)
		{
			std::cout << "m3 == m8" << std::endl;
		}
		else
		{
			std::cout << "m3 != m8" << std::endl;
		}		
		std::cout << "BlockTransMul m9" << std::endl;
		SquareMatrix<float>m9(m1);
		multiplyTransposedBlockMatrix(m1, m2, m9, 2);
		if (m3 == m9)
		{
			std::cout << "m3 == m9" << std::endl;
		}
		else
		{
			std::cout << "m3 != m9" << std::endl;
		}
		std::cout << "BlockOmp m10" << std::endl;
		SquareMatrix<float>m10(m1);
		multiplyBlockMatrixOmp(m1, m2, m10, 2);
		if (m3 == m10)
		{
			std::cout << "m3 == m10" << std::endl;
		}
		else
		{
			std::cout << "m3 != m10" << std::endl;
		}
		std::cout << "BlockTransOmp m11" << std::endl;
		SquareMatrix<float>m11(m1);
		multiplyTransposedBlockMatrixOmp(m1, m2, m11, 2);
		if (m3 == m11)
		{
			std::cout << "m3 == m11" << std::endl;
		}
		else
		{
			std::cout << "m3 != m11" << std::endl;
		}
		//___________________
	}
	{
		//�1
		std::cout << std::endl << "_________________" << std::endl << "L1" << std::endl;
		std::cout << std::endl;
		std::cout << "Symple" << std::endl;
		mulTime_test<float>(16, 4, multiplySquareMatrix<float>, 1E6);
		std::cout << "SSE" << std::endl;
		mulTime_test<float>(16, 4, multiplySquareMatrix4x4asm, 1E6);

		const size_t N_LOOPS = 1E5;
		const size_t DIMENSION = 4;
		const size_t DIMENSION2 = DIMENSION * DIMENSION;
		AlignedArrayPointer<float> m1(16, DIMENSION2);
		AlignedArrayPointer<float> m2(16, DIMENSION2);
		for (std::size_t i = 0; i < DIMENSION2; ++i)
		{
			m1[i] = m2[i] = i;
		}

		unsigned int N_THREADS;
#pragma omp parallel
		{
			if (!omp_get_thread_num())
			{
				N_THREADS = omp_get_num_threads();
			}
		}

		AlignedArrayPointer<float> m3(16, DIMENSION2 * N_THREADS);

		double time0 = omp_get_wtime();
#pragma omp parallel
		{
			int iThread = omp_get_thread_num();
			float* res = m3.getAlignedPointer() + DIMENSION2 * iThread;
#pragma omp for
			for (int q = 0; q < N_LOOPS; ++q)
			{
				multiplySquareMatrix(m1.getAlignedPointer(), m2.getAlignedPointer(), res, DIMENSION);
			}
		}
		printf("Omp C-function time: %lg\n", omp_get_wtime() - time0);

		time0 = omp_get_wtime();
#pragma omp parallel
		{
			int iThread = omp_get_thread_num();
			float* res = m3.getAlignedPointer() + DIMENSION2 * iThread;
#pragma omp for
			for (int q = 0; q < N_LOOPS; ++q)
			{
				multiplySquareMatrix4x4asm(m1.getAlignedPointer(), m2.getAlignedPointer(), res);
			}
		}
		printf("Asm C-function time: %lg\n", omp_get_wtime() - time0);

		//___________________
	}
	{
		//������� ��������� � ������� ��������� ������
		std::cout << std::endl << "_________________" << std::endl << "1024/1032" << std::endl;
		/*matrixMulTime_test<double>(64,1024, multiplySquareMatrix, 1);
		matrixMulTime_test<double>(64, 1026, multiplySquareMatrix, 1);
		matrixMulTime_test<double>(64, 1028, multiplySquareMatrix, 1);
		matrixMulTime_test<double>(64, 1030, multiplySquareMatrix, 1);
		matrixMulTime_test<double>(64, 1032, multiplySquareMatrix, 1);
		matrixMulTime_test<double>(64, 1034, multiplySquareMatrix, 1);*/
	}

	std::cout << std::endl;
	{
		//�3
		std::fstream file("times.csv");
		file.seekg(0, std::ios_base::end);
		std::cout << std::endl << "_________________" << std::endl << "L3" << std::endl;
		std::cout << "Symple" << std::endl;
		file << "Symple " << mulTime_test<double>(64, 1024, multiplySquareMatrix, 1) << std::endl;
		/*std::cout << "Omp" << std::endl;
		mulTime_test<double>(64, 1024, multiplySquareMatrixOmp, 1);*/
		std::cout << "Trans" << std::endl;
		file << "Trans " << mulTime_test<double>(64, 1024, multiplyTransposMatrix, 1) << std::endl;
		/*std::cout << "OmpTrans" << std::endl;
		mulTime_test<double>(64, 1024, multiplyTransposMatrixOmp, 1);*/
		for (int i = 2; i <= 1024; i *= 2)
		{
			std::cout << "____" << i << std::endl;
			std::cout << "Block " << std::endl;
			file << i << std::endl;
			file << "Block " << matrixBlockMulTime_test<double>(64, 1024, i, multiplyBlockMatrix, 1) << std::endl;
			std::cout << "BlockOmp" << std::endl;
			file << "BlockOmp " << matrixBlockMulTime_test<double>(64, 1024, i, multiplyBlockMatrixOmp, 1) << std::endl;
			std::cout << "BlockTrans" << std::endl;
			file << "BlockTrans " << matrixBlockMulTime_test<double>(64, 1024, i, multiplyTransposedBlockMatrix, 1) << std::endl;
			std::cout << "BlockTransOmp" << std::endl;
			file << "BlockTransOmp " << matrixBlockMulTime_test<double>(64, 1024, i, multiplyTransposedBlockMatrixOmp, 1) << std::endl;
		}
		file.close();
		//___________________
	}

	std::cout << "__________________________________________________" << std::endl << std::endl;
}

