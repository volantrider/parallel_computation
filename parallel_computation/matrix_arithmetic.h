#pragma once

#include <omp.h>

#include "square_matrix.h"

template <class T>
void multiplySquareMatrix(const SquareMatrix<T>& matrix1, const SquareMatrix<T>& matrix2, SquareMatrix<T>& result)
{
	assert(matrix1.getDimension() == matrix2.getDimension());
	assert(matrix1.getDimension() == result.getDimension());
	for (std::size_t i = 0; i < matrix1.getDimension(); ++i)
	{
		T* matrix1Row = matrix1[i];
		for (std::size_t j = 0; j < matrix1.getDimension(); ++j)
		{
			T f = 0;			
			for (std::size_t k = 0; k < matrix1.getDimension(); ++k)
			{
				f += matrix1Row[k] * matrix2[k][j];
			}
			result[i][j] = f;
		}
	}
}

template <class T>
void multiplySquareMatrix(const T* matrix1, const T* matrix2, T* result, std::size_t dimension)
{
	for (std::size_t i = 0; i < dimension; ++i)
	{
		const T* matrix1Row = matrix1 + i * dimension;
		for (std::size_t j = 0; j < dimension; ++j)
		{
			T f = 0;
			for (std::size_t k = 0; k < dimension; ++k)
			{
				f += matrix1Row[k] * matrix2[k * dimension + j];
			}
			result[i * dimension + j] = f;
		}
	}
}


//�������� ���� ������ 4*4 
//������� ����������� ����� ������������ �������
void multiplySquareMatrix4x4asm(const float* matrix1, const float* matrix2, float* result)
{
	//� ������ ����(intel) ����� ��������� ����������� � ��� � ���� ; � ��� � �� //
	//����� �������� ����� ����������� ������ � ���������� (�� �� ������ ��������)

	//����������� ��������� �������� ��� ������ ������������ ������ � ���������� �� 0 �� 15
	_asm
	{
		//� ������ ���� ����� ������������ �������� ���������� �� ��-����.
		mov eax, matrix2
		//movaps - ���������� 4 �����
		//[] - �������������� ������
		//�������� sse xmmX (����� ��������� 8) ������ �� 16 ���� ������
		movaps xmm0, [eax] //������ ������
		movaps xmm1, [eax + 16] //������ ������
		movaps xmm2, [eax + 32] //������ ������
		movaps xmm3, [eax + 48] //��������� ������
		// � ��������� ������ � �������� ������� (�������� ��������������)

		/*shufps d, s, imm8
		���������� � d ����� ������ �������:
		������� ��� ������������ �� ��������� d
		������� ��� �� ��������� s.
		imm8 ���������� ����� ��� ������ �������� ��������� � ��������� ����� ������������(������, ������ � ��)*/

		//����� ������� � �������� ������� �� ��������
		//��� ����� ������ ������� ���������� ������� � ��������� (����������)

		movaps xmm4, xmm0
		shufps xmm0, xmm1, 044h //0 - 0 1 4 5
		//�� �������(��� �������) 3 2 1 0, 7 6 5 4 -> 5 4 1 0. ������� 44h(01 00 01 00)
		//0 ��������� ����������� ��� 16��
		shufps xmm4, xmm1, 0EEh //4 - 2 3 6 7

		movaps xmm5, xmm2
		shufps xmm2, xmm3, 044h //2 - 13 12 9 8
		shufps xmm5, xmm3, 0EEh //5 - 13 14 11 10

		movaps xmm1, xmm0
		shufps xmm0, xmm2, 088h
		shufps xmm1, xmm2, 0DDh

		movaps xmm2, xmm4
		shufps xmm2, xmm5, 088h
		shufps xmm4, xmm5, 0DDh

		//xmm0 - ������ �������
		//xmm1 - ������ �������
		//xmm2 - ������ �������
		//xmm4 - ��������� �������

		mov ebx, result
		//addps ���������� ���������� 4 ��������(�������������� xmmX �������� ��� 4 float)
		mov eax, matrix1
		mov ecx, 4 //4 �������� �����

		//������������ ���������� ����� �������������� �������
		MUL_LOOP :
			movaps xmm3, [eax]
			movaps xmm5, xmm3
			movaps xmm6, xmm3

			mulps xmm5, xmm0// ������� ���1 �� ���1 0 4 16 36
			mulps xmm6, xmm1// ������� ���1 �� ���2 0 5 18 39

			movaps xmm7, xmm5// ������� ���1 �� ���1

			shufps xmm5, xmm6, 044h
			shufps xmm7, xmm6, 0EEh
			addps xmm5, xmm7// 16 40 1844 �������������

			// 6 � 7 �������� ��������
			// ������ ��� 3 � 4 �������
			movaps xmm6, xmm3// 1 ������

			mulps xmm3, xmm2// ������� ���1 �� ���1
			mulps xmm6, xmm4// ������� ���1 �� ���2

			movaps xmm7, xmm3// ������� ���1 �� ���1(�����)

			shufps xmm3, xmm6, 044h// 0 6 0 7
			shufps xmm7, xmm6, 0EEh// 20 42 22 45
			addps xmm3, xmm7// 20 48 22 52 �������������

			movaps xmm6, xmm5
			shufps xmm5, xmm3, 088h
			shufps xmm6, xmm3, 0DDh
			addps xmm5, xmm6

			movaps[ebx], xmm5
			add eax, 16
			add ebx, 16

			loop MUL_LOOP
	}
}

//�������� ���� ������ 4*4 
//������� ����������� ����� ������������ �������
void multiplySquareMatrix4x4asm(const SquareMatrix<float>& matrix1, const SquareMatrix<float>& matrix2,
	SquareMatrix<float>& result)
{
	assert(matrix1.getDimension() == 4);
	assert(matrix2.getDimension() == 4);
	assert(result.getDimension() == 4);

	const float* m1 = matrix1.getAlignedPointer();
	const float* m2 = matrix2.getAlignedPointer();
	float* m3 = result.getAlignedPointer();

	multiplySquareMatrix4x4asm(m1, m2, m3);
}

template <class T>
void multiplySquareMatrixOmp(const SquareMatrix<T>& matrix1, const SquareMatrix<T>& matrix2, SquareMatrix<T>& result)
{
	assert(matrix1.getDimension() == matrix2.getDimension());
	assert(matrix1.getDimension() == result.getDimension());
#pragma omp parallel for
	for (int i = 0; i < matrix1.getDimension(); ++i)
	{
		T* matrix1Row = matrix1[i];
		for (std::size_t j = 0; j < matrix1.getDimension(); ++j)
		{
			T f = 0;			
			for (std::size_t k = 0; k < matrix1.getDimension(); ++k)
			{
				f += matrix1Row[k] * matrix2[k][j];
			}
			result[i][j] = f;
		}
	}
}

template <class T>
void  multiplyTransposMatrix(const SquareMatrix<T>& matrix1, const SquareMatrix<T>& matrix2, SquareMatrix<T>& result)
{
	assert(matrix1.getDimension() == matrix2.getDimension());
	assert(matrix1.getDimension() == result.getDimension());
	const_cast<SquareMatrix<T>&>(matrix2).transpos();
	for (std::size_t i = 0; i < matrix1.getDimension(); ++i)
	{
		T* matrix1Row = matrix1[i];
		for (std::size_t j = 0; j < matrix1.getDimension(); ++j)
		{
			T f = 0;			
			T* matrix2Row = matrix2[j];
			for (std::size_t k = 0; k < matrix1.getDimension(); ++k)
			{
				f += matrix1Row[k] * matrix2Row[k];
			}
			result[i][j] = f;
		}
	}
	const_cast<SquareMatrix<T>&>(matrix2).transpos();
}

template <class T>
void  multiplyTransposMatrixOmp(const SquareMatrix<T>& matrix1, const SquareMatrix<T>& matrix2, SquareMatrix<T>& result)
{
	assert(matrix1.getDimension() == matrix2.getDimension());
	assert(matrix1.getDimension() == result.getDimension());
	const_cast<SquareMatrix<T>&>(matrix2).transpos();
#pragma omp parallel for
	for (int i = 0; i < matrix1.getDimension(); ++i)
	{
		T* matrix1Row = matrix1[i];
		for (std::size_t j = 0; j < matrix1.getDimension(); ++j)
		{
			T f = 0;
			T* matrix2Row = matrix2[j];
			for (std::size_t k = 0; k < matrix1.getDimension(); ++k)
			{
				f += matrix1Row[k] * matrix2Row[k];
			}
			result[i][j] = f;
		}
	}
	const_cast<SquareMatrix<T>&>(matrix2).transpos();
}

template <class T>
void multiplyBlockMatrix(const SquareMatrix<T>& matrix1, const SquareMatrix<T>& matrix2, 
	SquareMatrix<T>& result, std::size_t blockSize)
{
	assert(matrix1.getDimension() == matrix2.getDimension());
	assert(matrix1.getDimension() == result.getDimension());
	assert((matrix1.getDimension() % blockSize) == 0);
	for (std::size_t i = 0; i < result.getDimension(); ++i)
	{
		T* row = result[i];
		for (std::size_t j = 0; j < result.getDimension(); ++j)
		{
			row[j] = 0;
		}
	}
	for (int I = 0; I < matrix1.getDimension(); I += blockSize)
	{
		for (int J = 0; J < matrix1.getDimension(); J += blockSize)
		{
			T* c = result[I] + J;
			for (int K = 0; K < matrix1.getDimension(); K += blockSize)
			{
				T* a = matrix1[I] + K;
				T* b = matrix2[K] + J;
				for (int i = 0; i < blockSize; ++i)
				{
					for (int j = 0; j < blockSize; ++j)
					{
						T f = 0;
						for (int k = 0; k < blockSize; ++k)
						{
							f += a[i * matrix1.getDimension() + k] * b[k * matrix1.getDimension() + j];
						}
						c[i * matrix1.getDimension() + j] += f;
					}
				}
			}
		}

	}
}

template <class T>
void multiplyBlockMatrixOmp(const SquareMatrix<T>& matrix1, const SquareMatrix<T>& matrix2,
	SquareMatrix<T>& result, std::size_t blockSize)
{
	assert(matrix1.getDimension() == matrix2.getDimension());
	assert(matrix1.getDimension() == result.getDimension());
	assert((matrix1.getDimension() % blockSize) == 0);
#pragma omp parallel for
	for (int i = 0; i < result.getDimension(); ++i)
	{
		T* row = result[i];
		for (std::size_t j = 0; j < result.getDimension(); ++j)
		{
			row[j] = 0;
		}
	}
#pragma omp parallel for
	for (int I = 0; I < matrix1.getDimension(); I += blockSize)
	{
		for (int J = 0; J < matrix1.getDimension(); J += blockSize)
		{
			T* c = result[I] + J;
			for (int K = 0; K < matrix1.getDimension(); K += blockSize)
			{
				T* a = matrix1[I] + K;
				T* b = matrix2[K] + J;
				for (int i = 0; i < blockSize; ++i)
				{
					for (int j = 0; j < blockSize; ++j)
					{
						T f = 0;
						for (int k = 0; k < blockSize; ++k)
						{
							f += a[i * matrix1.getDimension() + k] * b[k * matrix1.getDimension() + j];
						}
						c[i * matrix1.getDimension() + j] += f;
					}
				}
			}
		}

	}
}

template <class T>
void multiplyTransposedBlockMatrix(const SquareMatrix<T>& matrix1, const SquareMatrix<T>& matrix2,
	SquareMatrix<T>& result, std::size_t blockSize)
{
	assert(matrix1.getDimension() == matrix2.getDimension());
	assert(matrix1.getDimension() == result.getDimension());
	assert((matrix1.getDimension() % blockSize) == 0);
	for (std::size_t i = 0; i < result.getDimension(); ++i)
	{
		T* row = result[i];
		for (std::size_t j = 0; j < result.getDimension(); ++j)
		{
			row[j] = 0;
		}
	}
	const_cast<SquareMatrix<T>&>(matrix2).transpos();
	for (int I = 0; I < matrix1.getDimension(); I += blockSize)
	{
		for (int J = 0; J < matrix1.getDimension(); J += blockSize)
		{
			T* c = result[I] + J;
			for (int K = 0; K < matrix1.getDimension(); K += blockSize)
			{
				T* a = matrix1[I] + K;
				T* b = matrix2[J] + K;
				for (int i = 0; i < blockSize; ++i)
				{
					for (int j = 0; j < blockSize; ++j)
					{
						T f = 0;
						for (int k = 0; k < blockSize; ++k)
						{
							f += a[i * matrix1.getDimension() + k] * b[j * matrix1.getDimension() + k];
						}
						c[i * matrix1.getDimension() + j] += f;
					}
				}
			}
		}

	}
	const_cast<SquareMatrix<T>&>(matrix2).transpos();
}


template <class T>
void multiplyTransposedBlockMatrixOmp(const SquareMatrix<T>& matrix1, const SquareMatrix<T>& matrix2,
	SquareMatrix<T>& result, std::size_t blockSize)
{
	assert(matrix1.getDimension() == matrix2.getDimension());
	assert(matrix1.getDimension() == result.getDimension());
	assert((matrix1.getDimension() % blockSize) == 0);
#pragma omp parallel for
	for (int i = 0; i < result.getDimension(); ++i)
	{
		T* row = result[i];
		for (std::size_t j = 0; j < result.getDimension(); ++j)
		{
			row[j] = 0;
		}
	}
	const_cast<SquareMatrix<T>&>(matrix2).transpos();
#pragma omp parallel for
	for (int I = 0; I < matrix1.getDimension(); I += blockSize)
	{
		for (int J = 0; J < matrix1.getDimension(); J += blockSize)
		{
			T* c = result[I] + J;
			for (int K = 0; K < matrix1.getDimension(); K += blockSize)
			{
				T* a = matrix1[I] + K;
				T* b = matrix2[J] + K;
				for (int i = 0; i < blockSize; ++i)
				{
					for (int j = 0; j < blockSize; ++j)
					{
						T f = 0;
						for (int k = 0; k < blockSize; ++k)
						{
							f += a[i * matrix1.getDimension() + k] * b[j * matrix1.getDimension() + k];
						}
						c[i * matrix1.getDimension() + j] += f;
					}
				}
			}
		}

	}
	const_cast<SquareMatrix<T>&>(matrix2).transpos();
}
